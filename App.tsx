import React, { useEffect, useState } from 'react';
import {
  Text,
} from 'react-native';

import WebView from 'react-native-webview';
import Server, { resolveAssetsPath } from '@dr.pogodin/react-native-static-server';

function App(): React.JSX.Element {

  const [origin, setOrigin] = useState('');

  useEffect(() => {
    let server = new Server({
     
      fileDir: resolveAssetsPath("webroot"),
     
    });
    (async () => {
     
      if (server) setOrigin(await server.start());

    })();
    return () => {
      setOrigin('');
      server.stop();
    }
  }, []);

  const url = origin //+ '/test.pdf'

if(!origin) {
  return (<Text>Loading</Text>)
} 
  return (
      
    <WebView 
    useWebView2
      source={{uri: url}} 
    />

  );
}

export default App;
